fetch('data.json')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        appendData(data);
    })
    .catch(function (err) {
        console.log('error: ' + err);
    });


function appendData(data) {
    let mainContainer = document.getElementById("posts");
    for (let i = 0; (i < 4) && data.length; i++) {
      let div = document.createElement("div");
  
      let profilePic = document.createElement("img");
      profilePic.src = data[i].profile_image;
      profilePic.setAttribute("class", "profilePic");
      div.appendChild(profilePic);
  
      let name = document.createElement("h3");
      name.innerHTML = data[i].name;
      name.setAttribute("class", "name");
      div.appendChild(name);
  
      let date = document.createElement("h5");
      date.setAttribute("class", "date");
      let months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ];
      let current_datetime = new Date(data[i].date);
      let formatted_date =
        current_datetime.getDate() +
        " " +
        months[current_datetime.getMonth()] +
        " " +
        current_datetime.getFullYear();
      date.innerHTML = formatted_date;
  
      div.appendChild(date);
  
      let instagramicon = document.createElement("span");
      instagramicon.innerHTML = `<svg width="30" height="25" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <img src="instagram-logo.svg"
      </svg>`;
      instagramicon.setAttribute("class", "instagramicon");
      div.appendChild(instagramicon);
  
      let img = document.createElement("img");
      img.src = data[i].image;
      img.setAttribute("class", "img");
      div.appendChild(img);
  
      if (!!data[i].caption.length) {
        let caption = document.createElement("p");
        caption.innerHTML = data[i].caption;
        caption.setAttribute("class", "caption");
        div.appendChild(caption);
      }
  
      let likeicon = document.createElement("button");
      likeicon.innerHTML = `<svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14.7617 3.26543C14.3999 2.90347 13.9703 2.61634 13.4976 2.42045C13.0248 2.22455 12.518 2.12372 12.0063 2.12372C11.4945 2.12372 10.9878 2.22455 10.515 2.42045C10.0422 2.61634 9.61263 2.90347 9.25085 3.26543L8.50001 4.01626L7.74918 3.26543C7.0184 2.53465 6.02725 2.1241 4.99376 2.1241C3.96028 2.1241 2.96913 2.53465 2.23835 3.26543C1.50756 3.99621 1.09702 4.98736 1.09702 6.02084C1.09702 7.05433 1.50756 8.04548 2.23835 8.77626L2.98918 9.52709L8.50001 15.0379L14.0108 9.52709L14.7617 8.77626C15.1236 8.41448 15.4108 7.98492 15.6067 7.51214C15.8026 7.03935 15.9034 6.53261 15.9034 6.02084C15.9034 5.50908 15.8026 5.00233 15.6067 4.52955C15.4108 4.05677 15.1236 3.62721 14.7617 3.26543V3.26543Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>`;
      likeicon.setAttribute("class", "likeicon");
      likeicon.setAttribute("id", "button" + i);
      div.appendChild(likeicon);
      let buttonfunction = (a) => {
        a;
      };
      buttonfunction("button" + [i]);
  
      let numberOfLikes = document.createElement("span");
      numberOfLikes.innerHTML = data[i].likes;
      numberOfLikes.setAttribute("class", "numberOfLikes");
      div.appendChild(numberOfLikes);
  
      mainContainer.appendChild(div);
      for (let item of document.getElementById("posts").children) {
          if (!item.classList.contains("initialized")) {
              item.classList.add("initialized");
              item.querySelector(".likeicon").onclick = function() {
                  if (!this.classList.contains("liked")) {
                      this.classList.add("liked");
                      this.parentNode.querySelector(".numberOfLikes").innerText = parseInt(this.parentNode.querySelector(".numberOfLikes").innerText) + 1;
                  } else {
                      this.classList.remove("liked");
                      this.parentNode.querySelector(".numberOfLikes").innerText = parseInt(this.parentNode.querySelector(".numberOfLikes").innerText) - 1;
                  }
              }
          }
      }
    }
  let jsonIndex = 0;
  
  document.getElementById("loadmore").onclick = function() {
      let collection = [];
      for (let i = 0; (i < 4) && (data.length > jsonIndex); i++) {
          console.log(collection.push(data[jsonIndex++]));
      }
      appendData(collection);
      if (jsonIndex >= data.length) this.style.display = "none";
  }

}
  
  



